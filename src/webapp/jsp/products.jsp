<%@page language="java" contentType="text/html; ISO-8859-1" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>Products</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<%--<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel" data-interval="2500">--%>
<%--<c:forEach items="${products}" var="product">--%>
<%--<ol class="carousel-indicators">--%>
<%--<li data-target="#myCarousel" data-slide-to="0" class="active"></li>--%>
<%--<li data-target="#myCarousel" data-slide-to="1"></li>--%>
<%--<li data-target="#myCarousel" data-slide-to="2"></li>--%>
<%--</ol>--%>
<%--<div class="carousel-inner">--%>
<%--<div class="item active">--%>
<%--<img class="img-responsive center-block" src="https://d2gk7xgygi98cy.cloudfront.net/6667-3-large.jpg"--%>
<%--alt="First slide" width="1200" height="900">--%>
<%--<div class="carousel-caption">--%>

<%--<p>${product.name}</p>--%>
<%--</div>--%>
<%--</div>--%>
<%--<div class="item">--%>
<%--<img class="img-responsive center-block"--%>
<%--src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRehNbT_YkmY3R5Cp-5QC-QYWR9OYv4T2DuBsEg5no5gBVHkPd2"--%>
<%--alt="Second slide" width="1200" height="900">--%>
<%--<div class="carousel-caption">--%>

<%--<p>${product.name}</p>--%>
<%--</div>--%>
<%--</div>--%>
<%--<div class="item">--%>
<%--<img class="img-responsive center-block"--%>
<%--src="https://www.cateringsupplier.co.uk/wp-content/uploads/2017/02/Macaroni.jpg" alt="Third slide"--%>
<%--width="1200" height="900">--%>
<%--<div class="carousel-caption">--%>

<%--<p>${product.name}</p>--%>
<%--</div>--%>
<%--</div>--%>
<%--</div>--%>
<%--<a class="left carousel-control" href="#carouselExampleSlidesOnly" data-slide="prev">--%>
<%--<span class="glyphicon glyphicon-chevron-left"></span>--%>
<%--<span class="sr-only">Previous</span>--%>
<%--</a>--%>
<%--<a class="right carousel-control" href="#carouselExampleSlidesOnly" data-slide="next">--%>
<%--<span class="glyphicon glyphicon-chevron-right"></span>--%>
<%--<span class="sr-only">Next</span>--%>
<%--</a>--%>
<%--</c:forEach>--%>
<%--</div>--%>
<div class="container">
    <h1 align="center">
    <p>Produkty</p>
    <form method="post" action="/products">
        Nazwa:
        <br/>
        <input type="text"name="name"><br/>
        cena:
        <br/>
        <input type="text"name="price"><br/><br/>
        <input type="submit" value="Dodaj"/>
    </form>
    </h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Nazwa</th>
            <th>Cena</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${products}" var="product">
            <tr>
                <td>${product.name}<br/></td>
                <td>${product.price}<br/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

</div>

</body>
</html>
