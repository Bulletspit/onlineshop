package com.sda;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ShopController", value = "/products")
public class ShopController extends HttpServlet {
    private static List<Product> products;

    static {
        Product chleb = new Product("Chleb", 2);
        Product kielbasa = new Product("Kiełbasa", 8);
        Product makaron = new Product("Makaron", 6);
        products = new ArrayList<>();
        products.add(chleb);
        products.add(kielbasa);
        products.add(makaron);
    }

    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        HttpSession session = httpServletRequest.getSession();
        httpServletRequest.setAttribute("products", products);
        httpServletRequest.getRequestDispatcher("products.jsp").forward(httpServletRequest, httpServletResponse);

    }

    @Override
    protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        HttpSession session = httpServletRequest.getSession();

        httpServletRequest.setAttribute("products", products);


        try {
            String name = httpServletRequest.getParameter("name");
            int price = Integer.parseInt(httpServletRequest.getParameter("price"));
            addProduct(name, price);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        httpServletRequest.getRequestDispatcher("products.jsp").forward(httpServletRequest, httpServletResponse);

    }

    void addProduct(String name, int price) throws NumberFormatException{

                Product product = new Product(name, price);
                products.add(product);

        }
    }

