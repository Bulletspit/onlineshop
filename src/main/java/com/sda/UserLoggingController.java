package com.sda;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "UserLoggingController", value = "/logging")
public class UserLoggingController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        httpServletRequest.getRequestDispatcher("register.jsp").forward(httpServletRequest, httpServletResponse);
    }

    @Override
    protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        String login = httpServletRequest.getParameter("login");
        String password = httpServletRequest.getParameter("password");
        UserAccount userAccount = logUser(login, password);
        if (userAccount != null) {
            HttpSession session = httpServletRequest.getSession();
            session.setAttribute("user", userAccount);
            httpServletResponse.sendRedirect("/home");
        } else {
            httpServletRequest.setAttribute("errorLogin", "Wrong credentials !");
            httpServletRequest.getRequestDispatcher("register.jsp").forward(httpServletRequest, httpServletResponse);
        }
    }

    UserAccount logUser(String login, String password) {
        UserDataBase userDataBase = UserDataBase.getInstance();
        UserAccount user = userDataBase.getUser(login, password);
        return user;

    }
}
