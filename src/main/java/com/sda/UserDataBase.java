package com.sda;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.Map;

public class UserDataBase {

    private static UserDataBase INSTANCE;


    private UserDataBase() {
    }

    void addUser(UserAccount userAccount) {
        EntityManager entityManager = DatabaseConnector.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(userAccount);
        transaction.commit();

    }

    UserAccount getUser(String login, String password)
    {
        try {
            EntityManager entityManager = DatabaseConnector.createEntityManager();
            Query query = entityManager.createQuery("select u from UserAccount u where  u.login =:login and u.password =:password");
            query.setParameter("login", login);
            query.setParameter("password", password);
            return (UserAccount) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }


    public static UserDataBase getInstance() {
        if (INSTANCE == null)
            INSTANCE = new UserDataBase();
        return INSTANCE;
    }


}
