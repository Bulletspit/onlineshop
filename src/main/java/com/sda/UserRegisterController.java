package com.sda;

import org.hibernate.NonUniqueResultException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "UserRegisterController", value = "/")
public class UserRegisterController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        httpServletRequest.getRequestDispatcher("register.jsp").forward(httpServletRequest, httpServletResponse);

    }

    @Override
    protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException, NonUniqueResultException {

        try {
            String name = httpServletRequest.getParameter("name");
            String surName = httpServletRequest.getParameter("surname");
            String login = httpServletRequest.getParameter("login");
            String password = httpServletRequest.getParameter("password");
            if (name.isEmpty() || surName.isEmpty() || login.isEmpty() || password.isEmpty()) {
                httpServletRequest.getRequestDispatcher("register.jsp").forward(httpServletRequest, httpServletResponse);
            }
            else {
                registerUser(name, surName, login, password);
                httpServletRequest.getRequestDispatcher("logging.jsp").forward(httpServletRequest, httpServletResponse);
            }
        } catch (NonUniqueResultException e) {
            e.printStackTrace();
            PrintWriter out = null;
            out.println("Doubles detected");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void registerUser(String name, String surName, String login, String password) {
        UserAccount userAccount = new UserAccount(name, surName, login, password);
        UserDataBase userDataBase = UserDataBase.getInstance();
        userDataBase.addUser(userAccount);
    }
}
